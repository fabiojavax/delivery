package com.cit.challenge.delivery.controller;

import com.cit.challenge.delivery.domain.Delivery;
import com.cit.challenge.delivery.domain.DeliveryStep;
import com.cit.challenge.delivery.exception.DeliveryAlreadyExistsException;
import com.cit.challenge.delivery.service.DeliveryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author fabiolemos
 */
@RestController
public class DeliveryController {

    @Resource
    private DeliveryService deliveryService;

    @RequestMapping(path = "/delivery",
            method = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity createDelivery(@RequestBody Delivery delivery) {

        try {

            deliveryService.createDelivery(delivery);

            return new ResponseEntity(HttpStatus.CREATED);

        } catch (DeliveryAlreadyExistsException dae) {

            return new ResponseEntity(HttpStatus.CONFLICT);

        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @RequestMapping(path = "/delivery",
            method = RequestMethod.DELETE,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity delete() {

        try {

            deliveryService.delete();

            return new ResponseEntity(HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @RequestMapping(path = "/delivery/{deliveryId}/step",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity getDeliverySteps(@PathVariable Integer deliveryId) {

        try {

            List<DeliveryStep>deliveryStepList = deliveryService.getDeliverySteps(deliveryId);

            if (deliveryStepList.isEmpty()) {
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(deliveryStepList, HttpStatus.OK);
            }

        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);

        }



    }




}

