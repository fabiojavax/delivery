package com.cit.challenge.delivery.controller;


import com.cit.challenge.delivery.Constants;
import com.cit.challenge.delivery.util.JSONReturnMessage;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author fabiolemos
 */
@RequestMapping("/healthcheck")
@RestController
public class HealthCheckController {

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public JSONReturnMessage healthCheck() {

        JSONReturnMessage returnMessage = new JSONReturnMessage();
        returnMessage.setDatetime(Constants.datetimeFormat.format(new Date()));

        long startTime = System.currentTimeMillis();

        returnMessage.setStatus(true);
        returnMessage.setMessage(Constants.SUCCESS_MESSAGE);

        returnMessage.setDuration(System.currentTimeMillis() - startTime);

        return returnMessage;

    }


}
