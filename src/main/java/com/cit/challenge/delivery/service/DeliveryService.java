package com.cit.challenge.delivery.service;

import com.cit.challenge.delivery.domain.Delivery;
import com.cit.challenge.delivery.domain.DeliveryPackage;
import com.cit.challenge.delivery.domain.DeliveryStep;
import com.cit.challenge.delivery.exception.DeliveryAlreadyExistsException;
import com.cit.challenge.delivery.repository.DeliveryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author fabiolemos
 */
@Service
public class DeliveryService {

    private static Logger LOGGER = Logger.getLogger(DeliveryService.class.getName());

    private final DeliveryRepository deliveryRepository;

    private Delivery delivery;
    private List<DeliveryStep> deliveryStepList;
    private int step = 0;

    private static String A_ZONE = "zona de abastecimento";
    private static String T_ZONE = "zona de transferência";
    private static String C_ZONE = "zona do caminhão";

    @Autowired
    public DeliveryService(DeliveryRepository deliveryRepository) {
        this.deliveryRepository = deliveryRepository;
    }

    public List<DeliveryStep> getDeliverySteps(Integer deliveryId) {

        delivery = deliveryRepository.findById(deliveryId);

        deliveryStepList = new ArrayList<>();

        if (delivery != null) {

            solve(delivery.getPackages().size(), A_ZONE, T_ZONE, C_ZONE);

            return deliveryStepList;

        }

        return deliveryStepList;

    }

    /**
     * Implementation of the Tower of Hanoi problem to solve the delivery problem
     * @param position package position, starting from the end
     * @param start the start zone
     * @param auxiliary the auxiliary zone
     * @param end the final destionazion zone
     */
    private void solve(int position, String start, String auxiliary, String end) {

        DeliveryPackage deliveryPackage = delivery.getPackages().get(position - 1);

        if (position == 1) {

            addStep(deliveryPackage, start, end);

        } else {

            solve(position - 1, start, end, auxiliary);
            addStep(deliveryPackage, start, end);
            solve(position - 1, auxiliary, start, end);

        }

    }

    private void addStep(DeliveryPackage deliveryPackage, String source, String destination) {
        DeliveryStep deliveryStep = new DeliveryStep();
        deliveryStep.setStep(++step);
        deliveryStep.setPackageId(deliveryPackage.getId());
        deliveryStep.setFrom(source);
        deliveryStep.setTo(destination);
        deliveryStepList.add(deliveryStep);
    }

    public void createDelivery(Delivery delivery) throws DeliveryAlreadyExistsException {

        if (deliveryRepository.findById(delivery.getDeliveryId()) != null) {

            throw new DeliveryAlreadyExistsException("delivery already exist");

        } else {

            LOGGER.info("creating delivery and packages...");
            deliveryRepository.create(delivery);

        }

    }

    public void delete() {

        deliveryRepository.delete();

    }





}