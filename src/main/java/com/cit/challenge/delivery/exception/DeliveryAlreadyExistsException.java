package com.cit.challenge.delivery.exception;

/**
 * @author fabiolemos
 */
public class DeliveryAlreadyExistsException extends Exception {

    public DeliveryAlreadyExistsException(String errorMessage) {
        super(errorMessage);
    }

}
