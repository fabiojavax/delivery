package com.cit.challenge.delivery.repository.mapper;


import com.cit.challenge.delivery.domain.Delivery;
import com.cit.challenge.delivery.domain.DeliveryPackage;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author fabiolemos
 */
public class DeliveryPackageRowMapper implements RowMapper<DeliveryPackage> {

    @Override
    public DeliveryPackage mapRow(ResultSet rs, int rowNum) throws SQLException {
        DeliveryPackage deliveryPackage = new DeliveryPackage();
        deliveryPackage.setId(rs.getInt("ID_PACKAGE"));
        deliveryPackage.setWeight(rs.getDouble("VL_WEIGHT"));
        return deliveryPackage;
    }

}
