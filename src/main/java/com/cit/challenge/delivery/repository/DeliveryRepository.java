package com.cit.challenge.delivery.repository;

import com.cit.challenge.delivery.domain.Delivery;
import com.cit.challenge.delivery.domain.DeliveryPackage;
import com.cit.challenge.delivery.repository.mapper.DeliveryPackageRowMapper;
import com.cit.challenge.delivery.repository.mapper.DeliveryRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

/**
 * @author fabiolemos
 */
@Repository
public class DeliveryRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DeliveryRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void create(final Delivery delivery) {

        final String sql = "INSERT INTO DELIVERY(ID_DELIVERY, ID_VEHICLE) VALUES (?, ?)";

        jdbcTemplate.update(connection -> {
            int position = 0;
            PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(++position, delivery.getDeliveryId());
            ps.setInt(++position, delivery.getVehicle());
            return ps;
        });

        for (DeliveryPackage deliveryPackage: delivery.getPackages()) {
            createPackage(delivery, deliveryPackage);
        }

    }

    private void createPackage(final Delivery delivery, final DeliveryPackage deliveryPackage) {

        final String sql = "INSERT INTO PACKAGE(ID_DELIVERY, ID_PACKAGE, VL_WEIGHT) VALUES (?, ?, ?)";

        jdbcTemplate.update(connection -> {
            int position = 0;
            PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(++position, delivery.getDeliveryId());
            ps.setInt(++position, deliveryPackage.getId());
            ps.setDouble(++position, deliveryPackage.getWeight());
            return ps;
        });

    }

    @Transactional(readOnly=true)
    public Delivery findById(int deliveryId) {

        try {

            Delivery delivery = jdbcTemplate.queryForObject(
                    "SELECT * FROM DELIVERY WHERE ID_DELIVERY = ?",
                    new Object[]{deliveryId},
                    new DeliveryRowMapper()
            );

            if (delivery != null) {
                delivery.setPackages(getPackages(deliveryId));
            }

            return delivery;

        } catch (Exception e) {

            return null;

        }

    }

    @Transactional(readOnly=true)
    public List<DeliveryPackage> getPackages(int deliveryId) {
        return jdbcTemplate.query(
                "SELECT * FROM PACKAGE WHERE ID_DELIVERY = ? ORDER BY VL_WEIGHT",
                new Object[]{deliveryId},
                new DeliveryPackageRowMapper()
        );
    }

    public void delete() {

        final String truncateDelivery = "TRUNCATE TABLE DELIVERY";

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(truncateDelivery, Statement.RETURN_GENERATED_KEYS);
            return ps;
        });

        final String truncatePackage = "TRUNCATE TABLE PACKAGE";

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(truncatePackage, Statement.RETURN_GENERATED_KEYS);
            return ps;
        });

    }


}


