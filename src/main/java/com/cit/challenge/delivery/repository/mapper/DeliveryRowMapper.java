package com.cit.challenge.delivery.repository.mapper;


import com.cit.challenge.delivery.domain.Delivery;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author fabiolemos
 */
public class DeliveryRowMapper implements RowMapper<Delivery> {

    @Override
    public Delivery mapRow(ResultSet rs, int rowNum) throws SQLException {
        Delivery delivery = new Delivery();
        delivery.setDeliveryId(rs.getInt("ID_DELIVERY"));
        delivery.setVehicle(rs.getInt("ID_VEHICLE"));
        return delivery;
    }

}
