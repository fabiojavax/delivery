package com.cit.challenge.delivery;

import java.text.SimpleDateFormat;

/**
 * @author fabiolemos
 */
public class Constants {

    public static SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    public static final String SUCCESS_MESSAGE = "operation successfully executed";
    public static final String TRANSACTION_ERROR = "could not perform this operation, please try again";

}

