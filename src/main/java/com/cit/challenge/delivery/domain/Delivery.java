package com.cit.challenge.delivery.domain;

import java.util.List;

public class Delivery {

    private Integer vehicle;
    private Integer deliveryId;
    private List<DeliveryPackage> packages;

    public Integer getVehicle() {
        return vehicle;
    }

    public void setVehicle(Integer vehicle) {
        this.vehicle = vehicle;
    }

    public Integer getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(Integer deliveryId) {
        this.deliveryId = deliveryId;
    }

    public List<DeliveryPackage> getPackages() {
        return packages;
    }

    public void setPackages(List<DeliveryPackage> packages) {
        this.packages = packages;
    }
}
